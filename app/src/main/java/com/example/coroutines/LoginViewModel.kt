package com.example.coroutines

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/*
Se define el dispatcher en el constructor para poder pasar el testDispatcher en los tests
 */
class LoginViewModel(private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO) : ViewModel() {

    private val _loginValid = MutableLiveData<Boolean>()
    val loginValid: LiveData<Boolean> get() = _loginValid


    fun onSubmitClicked(user: String, pass: String) {
        viewModelScope.launch {
            _loginValid.value = withContext(ioDispatcher) {
                validateLogin(user, pass)
            }
        }
    }

    private fun validateLogin(user: String, pass: String): Boolean {
        Thread.sleep(2000)
        return user.isNotEmpty() && pass.isNotEmpty()
    }
}