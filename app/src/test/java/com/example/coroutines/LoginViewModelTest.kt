package com.example.coroutines

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class LoginViewModelTest {

    // Obliga a los liveData a ejecutarse en el hilo del test en vez de ejecutarse en el hilo Main de Android
    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val coroutineTestRule = CoroutineTestRule()

    private lateinit var vm: LoginViewModel

    @Before
    fun setUp() {
        vm = LoginViewModel(coroutineTestRule.testDispatcher)
    }

    @After
    fun tearDown() {
    }

    @Test
    fun `success if user and pass are not empty`() {
        val observer = mock<Observer<Boolean>>()
        coroutineTestRule.testDispatcher.runBlockingTest {
            vm.loginValid.observeForever(observer)

            vm.onSubmitClicked("user", "pass")

            verify(observer).onChanged(true)
        }

    }

    @Test
    fun `error if user is empty`() = coroutineTestRule.testDispatcher.runBlockingTest {
        val observer = mock<Observer<Boolean>>()
        vm.loginValid.observeForever(observer)

        vm.onSubmitClicked("", "pass")

        verify(observer).onChanged(false)
    }

}
